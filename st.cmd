# Startup for TS2-010CRM:SC-IOC-920

# Load required modules
require essioc
require s7plc
require modbus
require calc

# Load standard IOC startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

# Load PLC specific startup script
iocshLoad("$(E3_CMD_TOP)/iocsh/ts2_010crm_wtrc_plc_001.iocsh", "DBDIR=$(E3_CMD_TOP)/db/, MODVERSION=$(IOCVERSION=)")

